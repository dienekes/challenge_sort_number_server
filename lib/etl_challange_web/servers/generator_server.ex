defmodule EtlChallangeWeb.GeneratorServer do
  @moduledoc """
  Genserver that generates a sequence of 1.000.000 numbers.

  The numbers and the sequence are always the same, even after a Genserver restart.

  To access the sequence you must use the `get/1` function. 
  """
  def start_link() do
    GenServer.start_link(__MODULE__, :ok, name: :generator_server)
  end

  @doc """
  Allows paging through the numbers sequence.

  - The page number must be a positive integer.
  - Each page has 100 numbers. So the total number of pages is 10.000. 
  - If you try to get a page number higher than 10.000, the function will return an empty list.
  - If you try to get a page number lower than 1, the function will return nil.

  ## Exemple
      iex> EtlChallangeWeb.GeneratorServer.start_link() # or start it in the application supervisor
      iex> EtlChallangeWeb.GeneratorServer.get(1)
      [0.4181707133672159, 0.4446014940576127, 0.3364303523294636,
       0.4135426519101487, 0.2245623856417824, 0.6501239000539522,
       0.5998745794215936, 0.6354200131296106, 0.5000394301387743,
       0.11919252546814178, 0.015621683926585785, 0.038535707538389585,...]
  """
  def get(page) when page < 1, do: nil
  def get(page), do: GenServer.call(:generator_server, {:get, page})

  ## Server Callbacks
  def init(:ok) do
    :rand.seed(:exsplus, {101, 102, 99})
    list = :array.new(1_000_000)
    {:ok, generate(0, list)}
  end

  defp generate(1_000_000, list), do: list
  defp generate(total, list), do: generate(total + 1, :array.set(total, :rand.uniform(), list))

  def handle_call({:get, page}, _from, state) do
    start = (page - 1) * 100
    final = start + 100
    {:reply, select(state, start, final), state}
  end

  defp select(array, start, final), do: select(array, start, final, [])

  defp select(_, start, _, current) when start >= 1_000_000, do: current

  defp select(_, final, final, current), do: current

  defp select(array, start, final, current) do
    new_current = [:array.get(start, array) | current]
    select(array, start + 1, final, new_current)
  end
end
