defmodule EtlChallangeWeb.Controller.NumberController do
  use EtlChallangeWeb, :controller
  alias EtlChallangeWeb.GeneratorServer

  def index(conn, params) do
    case show_random_error? do
      true -> conn |> put_status(500) |> json(%{error: "Simulated internal error"})
      false -> json(conn, get_numbers(params))
    end
  end

  defp show_random_error?() do
    :rand.uniform() < 0.01
  end

  defp get_numbers(%{"page" => ""}), do: GeneratorServer.get(1)

  defp get_numbers(%{"page" => page}) do
    case Integer.parse(page) do
      {num, _} -> %{numbers: GeneratorServer.get(num)}
      _ -> %{error: "Page must be integer"}
    end
  end

  defp get_numbers(_), do: GeneratorServer.get(1)
end
