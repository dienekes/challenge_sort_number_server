defmodule EtlChallangeWeb.Router do
  use EtlChallangeWeb, :router

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/api", EtlChallangeWeb do
    pipe_through(:api)
    resources("/numbers", Controller.NumberController, only: [:index])
  end
end
