# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :etl_challange,
  ecto_repos: [EtlChallange.Repo]

# Configures the endpoint
config :etl_challange, EtlChallangeWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "ysXRHpJWI2Hp6VGiBdE+YfPpbDLHI+leos/Hib+wMdQPbps/RABcthiXVjvlLt9M",
  render_errors: [view: EtlChallangeWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: EtlChallange.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
